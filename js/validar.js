(function(){
    //variables declaradas
    console.log("Ingreso al script");

    var patUsuario = /^[A-Za-z0-9]{6,}$/;
    
    
    var errIngreso = document.createTextNode("El campo usuario o clave es incorrecto");
    


    var form = document.getElementsByName('formulario')[0],
        elementos = form.elements,
        bot = document.getElementById('boton'),
        nombre= document.getElementById('nombre');
        clave= document.getElementById('clave');
        
        
   
    //validacion nombre usuario

    var validarUsuario= function(e){
        if(form.nombre.value == 0 ){
            document.getElementById('error').innerHTML = "Usuario o clave inv&aacute;lidos.";
            e.preventDefault();
        }
        else if(form.nombre.value.length < 6 ){
            document.getElementById('error').innerHTML = "Usuario o clave inv&aacute;lidos.";
            console.log(nombre.value);
            e.preventDefault();
        }
        else if(! patUsuario.test(form.nombre.value)){
            document.getElementById('error').innerHTML = "Usuario o clave inv&aacute;lidos.";
            e.preventDefault();
        }
    };

    var validarClave= function(e){
        
        if(form.clave.value == 0 ){
            document.getElementById('error').innerHTML = "Usuario o clave inv&aacute;lidos.";
            e.preventDefault();
        }
        else if(form.clave.value.length < 6 ){
            document.getElementById('error').innerHTML = "Usuario o clave inv&aacute;lidos.";
            e.preventDefault();
        }
        else if(! patUsuario.test(form.clave.value)){
            e.preventDefault();
            document.getElementById('error').innerHTML = "Usuario o clave inv&aacute;lidos.";
        }
    };

    

    //Funcion principal que se ejecuta al presionar el boton 'Registrarse'.

    var validar = function(e){
        validarUsuario(e);
        validarClave(e);
    };
    

    //Evento del boton 'Registrarse'.

    form.addEventListener("submit",validar);

}());
