<?php
	require_once('BD.php'); 
	require_once('controller/Comercio_Controller.php');
	require_once('core/routes.php'); 
	// la variable controller guarda el nombre del controlador y action guarda la acción por ejemplo registrar 
	//si la variable controller y action son pasadas por la url desde inicio.php entran en el if
	if (isset($_GET['controller']) && isset($_GET['action'])) {
		$controller= cargarControlador($_GET['controller']);		
		$action = $_GET['action'];
		//echo($action);die;
		cargarAccion($controller,$action);
	} else {
		$controller= "ComercioController";
		$action='inicio';
		require_once('../comercio_git/vistas/inicio.php');
	}	
	//carga la vista inicio.php
	
	
?>