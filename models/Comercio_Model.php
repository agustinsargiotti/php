<?php
require_once("../comercio_git/BD.php");
class ComercioModel{
    private $db;
    private $conn;

    public function __construct(){
        $db = new conexionBd();
        $conn = $db->conectar();
    }

    public static function validarIngreso($usuario,$clave){
        $db = new conexionBd();
        $conn = $db->conectar();
        $sql="select * from usuarios where nombreusuario='$usuario' and clave='$clave'";
        $arr_rta = array();
        $resultado = mysqli_query($conn,$sql);
        if ($resultado != null && $resultado!=""){
            while ($value = $resultado->fetch_assoc()) {
                $arr_rta["rol"] = $value["rol"];
                $arr_rta["id_usuario"] = $value["id"];
                return $arr_rta;
            }    
        }
    }

    public static function opcionesMenuComida(){
        $db = new conexionBd();
        $conn = $db->conectar();
        $sql="select * from items_menu where tipo='COMIDA'";
        $resultado = mysqli_query($conn,$sql);
        $array_comida = array();
        $i=0;
        while ($value = $resultado->fetch_assoc()) {
            $array_comida[$i]["id"]=$value["id"];
            $array_comida[$i]["nombre"]=$value["nombre"];
            $array_comida[$i]["precio"]=$value["precio"];
            $array_comida[$i]["foto"]=$value["foto"];
            $i++;
        }
        return $array_comida;
    }


    public static function opcionesMenuBebida(){
        $db = new conexionBd();
        $conn = $db->conectar();
        $sql="select * from items_menu where tipo='BEBIDA'";
        $resultado = mysqli_query($conn,$sql);
        $array_bebida = array();
        $i=0;
        while ($value = $resultado->fetch_assoc()) {
            $array_bebida[$i]["id"]=$value["id"];
            $array_bebida[$i]["nombre"]=$value["nombre"];
            $array_bebida[$i]["precio"]=$value["precio"];
            $array_bebida[$i]["foto"]=$value["foto"];
            $i++;
        }
        return $array_bebida;
    }

    public static function insertar_pedido($id_usuario,$id_item,$nro_mesa,$descripcion){
        $db = new conexionBd();
        $conn = $db->conectar();
        $sql="INSERT INTO pedidos (nromesa,idMozo,idItemMenu,comentarios,fechayhora,entregado)
                VALUES ('$nro_mesa','$id_usuario','$id_item','$descripcion',now(),0)";
        $resultado = mysqli_query($conn,$sql);
        return $resultado;
    }
    
}