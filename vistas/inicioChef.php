<html>

<head>
    <!-- Importo libreria de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Nuestra hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilosChefBartenderCaja.css">
    <title> Inicio Cocinero </title>

</head>
<header>
    <h1><span id="Confiteria" name="Confiteria">Confiteria</span> El Club</h1>
    <p>Una nueva forma de cuidarnos!</p>
</header>

<body class="usuariosBody">
    <div class="botonCerrar">
        <a href=".?controller=Comercio&action=cerrarSesion">Cerrar Sesi&oacute;n</a>
    </div>

    <img src="../comercio_git/Imagenes/ChefPerfil.jpg" alt="Imagen Chef" class="imgPerfil">
    
    <h2>BIENVENIDO <p style="font-size: small;">Que tengas una linda jornada :)</p></h2>

    <h3><span>A</span> Entregar:</h3>
    <ol class="listadoPedidos">
        <li>
            <div class="contenedor_pedido_chef">
                <h5>Hamburguesa Con Papas</h5>
                <p>Doble cheddar sin condimentos</p>
                <p>Mozo: Fabian</p>
                <input type="button"  value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_chef">
                <h5>Milanesa Napolitana</h5>
                <p>Sin tomate</p>
                <p>Mozo: Julia</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_chef">
                <h5>Pato A la Naranja</h5>
                <p>A punto</p>
                <p>Mozo: Micaela</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_chef">
                <h5>Picada Marinera</h5>
                <p>Tama&ntilde;o grande</p>
                <p>Mozo: Fabian</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_chef">
                <h5>Rabas Con Papas R&uacute;sticas</h5>
                <p>Agregar Lim&oacute;n y Mayonesa</p>
                <p>Mozo: Julia</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
    </ol>
    <a href="#">Anterior</a>
    <a href="#">Siguiente</a>
</body>

<footer> NO OLVIDES TU TAPABOCAS  </footer> 


        
        
</html>
