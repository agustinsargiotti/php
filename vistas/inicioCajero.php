<html>

<head>
    <!-- Importo libreria de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Nuestra hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilosChefBartenderCaja.css">
    <title> Inicio Cajero </title>
</head>

<header>
    <h1><span id="Confiteria" name="Confiteria">Confiteria</span> El Club</h1>
    <p>Una nueva forma de cuidarnos!</p>
</header>

<body class="usuariosBody">
    <div class="botonCerrar">
        <a href=".?controller=Comercio&action=cerrarSesion">Cerrar Sesi&oacute;n</a>
    </div>
    
    <img src="../comercio_git/Imagenes/restaurante2.jpg" alt="Imagen Chef" class="imgPerfil">
    
    <h2>BIENVENIDO <p style="font-size: small;">Que tengas una linda jornada :)</p></h2>

    <h3>Mesas <span>Activas</span></h3>
    <ol class="listadoPedidos">
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 1</h5>
                <p>Mozo: Juan P&eacute;ez</p>
                <p>Hora Inicio : 20:05 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 9</h5>
                <p>Mozo: Pedro P&eacute;rez</p>
                <p>Hora Inicio : 20:10 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 2</h5>
                <p>Mozo: Juan M&eacute;ndez</p>
                <p>Hora Inicio : 20:15 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 8</h5>
                <p>Mozo: Juan P&eacute;z</p>
                <p>Hora Inicio : 20:25 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 5</h5>
                <p>Mozo: Juan M&eacute;ndez</p>
                <p>Hora Inicio : 21:05 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 10</h5>
                <p>Mozo: Juan P&eacute;ez</p>
                <p>Hora Inicio : 21:07 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 14</h5>
                <p>Mozo: Pedro P&eacute;rez</p>
                <p>Hora Inicio : 21:10 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 3</h5>
                <p>Mozo: Juan M&eacute;ndez</p>
                <p>Hora Inicio : 21:15 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 6</h5>
                <p>Mozo: Juan P&eacute;z</p>
                <p>Hora Inicio : 22:25 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_caja">
                <h5>Mesa 12</h5>
                <p>Mozo: Juan M&eacute;ndez</p>
                <p>Hora Inicio : 23:05 Hs</p>
                <a href="listaMesa.php">Ver Detalle</a>
            </div>
        </li>
    </ol>
    <a href="#">Siguiente</a>
    <a href="#">Anterior</a>
    

</body>
<footer> NO OLVIDES TU TAPABOCAS  </footer> 
        
</html>
