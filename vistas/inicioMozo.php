<html>
  
<head>
    <title> Confiteria El Club </title>
    
    <!-- Importo libreria de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Nuestra hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilosFormPedidos.css">
    
    <!-- Importamos Font Awesome para usar iconos -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

<header>
<h1><span id="Confiteria" name="Confiteria">Confiteria</span> El Club</h1>
<p>Una nueva forma de cuidarnos!</p>
</header>

<body class="usuariosBody">
    
    <div class="botonCerrar">
        <a href=".?controller=Comercio&action=cerrarSesion">Cerrar Sesi&oacute;n</a>
    </div>

    <img src="../comercio_git/Imagenes/MozoPerfil.jpg" alt="Imagen Mozo" class="imgPerfil">
    
    <h2 class="bienvenido">BIENVENIDO <p style="font-size: small;">Que tengas una linda jornada :)</p></h2>
    
    <div class="contenedorLogo">
        <i class="fas fa-utensils" id="tenedores" ></i>
        <span>.</span>
        <i class="fas fa-cocktail" id="tenedores"></i>
    </div>
    <div class="contenedorPedido">
        <h3>Nuevo pedido</h3> 
        <button type="button" id="btnModal" class="ingresoBot" style="margin-left:35%; margin-right:auto;width:30%;">Comida</button> 
        <br><br>
        <button type="button" id="btnModal2" class="ingresoBot" style="margin-left:35%; margin-right:auto;width:30%;">Bebida</button> 
    </div>
  

    
</body>


<footer> NO OLVIDES TU TAPABOCAS  </footer> 

<div id="pedidoComida" class="contenedorModal">
    <div class="contenido">
        <span class="close">X</span>
        <h2 class="tituloModal">Al <span>Chef:</span> </h2>
        <p id="errPedido" class="errPedido"name="errPedido"></p>
        <form action=".?controller=Comercio&action=pedirComida" method="POST" id="formComida" name="formComida">
            <div>
                
                <select name="pedido" id="pedido" class="pedido">
                    <option value="">Seleccione Comida..</option>
                    <?php
                        if(isset($menuComida) && $menuComida!=""){
                            foreach ($menuComida as $value) {
                                echo "<option value=".$value["id"].">".$value["nombre"]."</option>";
                            }
                        }
                    ?>
                </select>
                
                <select name="nroMesa" id="nroMesa" class="pedido">
                    <option value="">Seleccione Mesa..</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>

                <textarea name="descripcion" id="descripcion" cols="29" rows="8" placeholder="Descripcion.." ></textarea>

                <input type="submit" class="ingresoBot" name="" id="pedirComida" value="Pedir">
            </div>   
        </form>
    </div>
</div>


<div id="pedidoBebida" class="contenedorModal">
    <div class="contenido">
        <span class="close">X</span>
        <h2 class="tituloModal">Al <span>Bartender:</span> </h2>
        <p id="errPedidob" class="errPedidob"name="errPedidob"></p> 
        <form action=".?controller=Comercio&action=pedirBebida" method="POST" id="formBebida" name="formBebida">
            <div>
                <select name="pedidob" id="pedidob" class="pedido">
                    <option value="">Seleccione Bebida..</option>
                    <?php
                        if(isset($menuBebida) && $menuBebida!=""){
                            foreach ($menuBebida as $values) {
                                echo "<option value=".$values["id"].">".$values["nombre"]."</option>";
                            }
                        }
                    ?>
                </select>
                        
                <select name="nroMesab" id="nroMesab" class="pedido">
                    <option value="">Seleccione Mesa..</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>

                <textarea name="descripcionb" id="descripcionb" cols="29" rows="8" placeholder="Descripcion.." ></textarea>
                
                
                <input type="submit" class="ingresoBot" name="" id="pedirBebida" value="Pedir">
            </div>   
        </form>
    </div>
</div>
   
<script src="../comercio_git/js/modal.js"></script>
<script src="../comercio_git/js/validarpedidos.js"></script>



</html>