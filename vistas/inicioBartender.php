<html>

<head>
    <!-- Importo libreria de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Nuestra hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../comercio_git/css/estilosChefBartenderCaja.css">
    <title> Inicio Bartender </title>

</head>
<header>
    <h1><span id="Confiteria" name="Confiteria">Confiteria</span> El Club</h1>
    <p>Una nueva forma de cuidarnos!</p>
</header>

<body class="usuariosBody">
    
    <div class="botonCerrar">
        <a href=".?controller=Comercio&action=cerrarSesion">Cerrar Sesi&oacute;n</a>
    </div>
    
    <img src="../comercio_git/Imagenes/BartenderPerfil.jpeg" alt="Imagen Bartender" class="imgPerfil">
    
    <h2>BIENVENIDO <?php if (isset($_POST['nombreusuario'])){echo $_POST['nombreusuario'];}   ?> <p style="font-size: small;">Que tengas una linda jornada :)</p></h2>

    <h3><span>A</span> Entregar:</h3>
    <ol class="listadoPedidos">
        <li>
            <div class="contenedor_pedido_bar">
                <h5>Cerveza Cornona</h5>
                <p>Bien fria</p>
                <p>Mozo: Fabian</p>
                <input type="button"  value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_bar">
                <h5>Coca-Cola</h5>
                <p>Zero,con hielo</p>
                <p>Mozo: Julia</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_bar">
                <h5>Coca-Cola Chica X4</h5>      
                <p></p>          
                <p>Mozo: Micaela</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_bar">
                <h5>Vino Malbec "Latitud" 3/4 </h5>
                <p>Natural con Naranja</p>
                <p>Mozo: Fabian</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
        <li>
            <div class="contenedor_pedido_bar">
                <h5>Fernet-Vaso Chico-X4</h5>
                <p>Suave</p>
                <p>Mozo: Julia</p>
                <input type="button" value="Entregar" name="" id="">
            </div>
        </li>
    </ol>
    <a href="#">Siguiente</a>
    <a href="#">Anterior</a>
    
</body>

<footer> NO OLVIDES TU TAPABOCAS   </footer> 

        
        
</html>
