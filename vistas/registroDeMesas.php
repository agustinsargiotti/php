<html>

<head>
    <!-- Importo libreria de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Nuestra hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosChefBartenderCaja.css">
    <title> Registro De Mesas</title>

</head>
<header>
    <h1><span id="Confiteria" name="Confiteria">Confiteria</span> El Club</h1>
    <p>Una nueva forma de cuidarnos!</p>
</header>

<body class="usuariosBody">
            <div class="botonCerrar">
                <a href="../index.php">Cerrar Sesi&oacute;n</a>
            </div>
            
            <img src="../Imagenes/restaurante2.jpg" alt="Imagen Chef" class="imgPerfil">
            
            <h2>BIENVENIDO <p style="font-size: small;">Que tengas una linda jornada :)</p></h2>

            <h3>Mesas <span>Cerradas</span></h3>
           
            <ol class="listadoPedidos">
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
                <li>
                    <div class="contenedor_pedido_caja">
                        <h5>Mozo : Juan P&eacute;rez</h5>
                        <p>Fecha: 21/05/2020 </p>
                        <p>Hora Inicio : 20:05 Hs</p>
                        <p>Hora cierre : 15:19 </p>
                        <p>Total : 1370,00 $</p>
                        <a href="mesaCerrada.php">Ver Consumos</a>
                    </div>
                </li>
            </ol>
            <a href="#">Siguiente</a>
            <a href="#">Anterior</a>
</body>
<footer> NO OLVIDES TU TAPABOCAS </footer> 
</html>
