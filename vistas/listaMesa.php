<html>

<head>
    <!-- Importo libreria de Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Nuestra hoja de estilos -->
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosResumenMesa.css">
    <title> Detalle De Mesa</title>

</head>
<header>
    <h1><span id="Confiteria" name="Confiteria">Confiteria</span> El Club</h1>
    <p>Una nueva forma de cuidarnos!</p>
</header>

<body class="usuariosBody">
    <div class="botonCerrar">
        <a href="../index.php">Cerrar Sesi&oacute;n</a>
    </div>

    <img src="../Imagenes/restaurante2.jpg" alt="Imagen Chef" class="imgPerfil">
    
    <h2>Detalle Mesa</h2>
    
    <div class="contenedorDetalle">
      <div class="detalleConsumo">
        <h3>Milanesa Con Guarnici&oacute;n- 4 Unidades</h3>
        <img src="../Imagenes/milanesa.jpg" alt="Consumo">
        <p>SubTotal : $1000</p>
      </div>  
      
      <div class="detalleConsumo">
        <h3>Cerveza Corona- 2 Unidades</h3>
        <img src="../Imagenes/corona.jpg" alt="Consumo">
        <p>SubTotal : $390</p>
      </div>       
               
      <div class="detalleConsumo">
        <h3>Coca-Cola 600ml - 2 Unidades</h3>
        <img src="../Imagenes/cocaCola.jpg" alt="Consumo">
        <p>SubTotal : $200</p>
      </div>   

      <h3>Total : $1590</h3>
      <a href="../vistas/registroDeMesas.php">CERRAR MESA</a>                      
    </div>
           
                             
</body>
<footer> NO OLVIDES TU TAPABOCAS </footer> 


        
        
</html>
