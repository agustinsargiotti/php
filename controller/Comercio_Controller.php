<?php
require_once("../comercio_git/models/Comercio_Model.php");
class ComercioController{

    public function __construct(){
    }

    public function inicio(){
        require_once('../comercio_git/vistas/inicio.php');
    }

    public function validar(){
        $usuario = $_POST['nombre'];
        $clave = $_POST['clave'];
        $comercioModel= new ComercioModel();
        $rta = $comercioModel::validarIngreso($usuario,$clave);
        if($rta != "" && $rta != null){
            
            session_start();
            $_SESSION["usuario"]=$rta["id_usuario"];
            
            if($rta["rol"]=='MOZO'){
                $this->inicio_mozo();
            }
            if($rta["rol"]=='BARTENDER'){
                require_once('../comercio_git/vistas/inicioBartender.php');
            }
            if($rta["rol"]=='CAJERO'){
                require_once('../comercio_git/vistas/inicioCajero.php');
            }
            if($rta["rol"]=='COCINERO'){
                require_once('../comercio_git/vistas/inicioChef.php');
            }
        }
        else{
            require_once('../comercio_git/vistas/inicio.php');
        }
    }

    public function inicio_mozo(){
        $comercioModel= new ComercioModel();
        $menuComida = $comercioModel::opcionesMenuComida();
        $menuBebida = $comercioModel::opcionesMenuBebida();
        require_once('../comercio_git/vistas/inicioMozo.php');
    }
    

    public function pedirComida(){
        session_start();
        $id_usuario = $_SESSION["usuario"];
        $id_item = $_POST["pedido"];
        $nro_mesa = $_POST["nroMesa"];
        $descripcion = $_POST["descripcion"];
        $comercioModel= new ComercioModel();
        $comercioModel::insertar_pedido($id_usuario,$id_item,$nro_mesa,$descripcion);
        $this->inicio_mozo();
    }

    public function pedirBebida(){
        session_start();
        $id_usuario = $_SESSION["usuario"];
        $id_item = $_POST["pedidob"];
        $nro_mesa = $_POST["nroMesab"];
        $descripcion = $_POST["descripcionb"];
        $comercioModel= new ComercioModel();
        $comercioModel::insertar_pedido($id_usuario,$id_item,$nro_mesa,$descripcion);
        $this->inicio_mozo();
    }

    public function cerrarSesion(){
        session_start();
        session_destroy();
        require_once('../comercio_git/vistas/inicio.php');
    }

}
