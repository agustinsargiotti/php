<?php

	function cargarControlador($controlador){
		$nombre_controlador = $controlador."Controller";
		$archivo_controlador =	$controlador."_Controller.php";

		if(!is_file($archivo_controlador)){
			$archivo_controlador = 'controller/Comercio_Controller.php';
		}
		//echo($nombre_controlador);
		require_once($archivo_controlador);
		$control = new $nombre_controlador();
		return $control;
	}

	function cargarAccion($controlador,$accion){
		if(isset($accion) && method_exists($controlador,$accion)){
			$controlador->$accion();
		}
		else{
			$controlador->inicio();
		}
	}
?>